import driverSetup.baseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.Hrms.homeScreen;
import pages.Hrms.loginScreen;
import pages.Hrms.returnScreen;

public class returnTest extends baseClass {

    WebDriver driver;

    baseClass base;

    homeScreen home;

    loginScreen login;

    returnScreen rtn;

    String email = "employee@capital.com", password = "password";

    public returnTest () {

        base = PageFactory.initElements(driver, baseClass.class);

    }


    @Test(priority = 3 )
    public  void returning () throws InterruptedException {

        home = base.initializeDriver();

        rtn = home.clickReturn();

        rtn.details(email, password);

    }

}
