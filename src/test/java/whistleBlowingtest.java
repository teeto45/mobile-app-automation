import com.github.dhiraj072.randomwordgenerator.RandomWordGenerator;
import driverSetup.baseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.Hrms.homeScreen;
import pages.Hrms.whistleScreen;

public class whistleBlowingtest extends baseClass {

    WebDriver driver;

    baseClass base;

    homeScreen home;

    whistleScreen whistle;

    String Subject = RandomWordGenerator.getRandomWord();

    String body = RandomWordGenerator.getRandomWord();

    public whistleBlowingtest () {


        base = PageFactory.initElements(driver, baseClass.class);

    }

    @Test(priority = 4)
    public void blowWhistle () {

        home = base.initializeDriver();

        whistle = home.whistleblowing();

        whistle.setWhistleType(Subject,body);

    }

}
