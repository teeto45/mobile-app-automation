import driverSetup.baseClass;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Hrms.dashBoard;
import pages.Hrms.homeScreen;
import pages.Hrms.loginScreen;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.rmi.UnexpectedException;

public class loginTest extends baseClass {

    WebDriver driver;

    baseClass base;

    homeScreen home;

    loginScreen login;

    dashBoard dash;

    String userId = "employee001", password = "password";

    public loginTest() {

        base = PageFactory.initElements(driver, baseClass.class);
    }



    @Test
    public  void login () {

        home = base.initializeDriver();

        login = home.clickLogin();

        dash = login.setLogin(userId,password);

        Assert.assertEquals ("Hi Emmanuel", dash.validateUser());
    }


}
