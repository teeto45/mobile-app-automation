import driverSetup.baseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.Hrms.forgotScreen;
import pages.Hrms.homeScreen;
import pages.Hrms.loginScreen;

public class forgetPasswordTest extends baseClass  {

    WebDriver driver;

    baseClass base;

    homeScreen home;

    loginScreen login;

    forgotScreen forgot;

    String emailID = "test@gmail.com";


    public  forgetPasswordTest () {

        base = PageFactory.initElements( driver, baseClass.class);

    }

    @Test(priority = 2)
    public void recoverPassword () {

        home = base.initializeDriver();

        login = home.clickLogin();

        forgot = login.passwordRecovery();

        forgot.recoverPassword(emailID);

    }

}
