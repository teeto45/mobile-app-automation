package pages.Hrms;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class homeScreen  {

    public WebDriver driver;

    public homeScreen(WebDriver driver) {

//      A "constructor" is a function that inherits the name of the class and is used to initialize objects.

        this.driver = driver;

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @FindBy( xpath = "//*[@text='SKIP']") WebElement skipToLogin;

    @FindBy ( xpath = "//*[@class='android.view.ViewGroup' and ./*[@text='Log in to your account']]") WebElement Login;

    @FindBy ( xpath = "//*[@class='android.view.ViewGroup' and ./*[@text='Return from Leave']]") WebElement rtnlvbtn;

    @FindBy (xpath = "//*[@text='Whistle Blowing']") WebElement Blow;

    public loginScreen clickLogin () {

        skipToLogin.click();

        Login.click();

        return new loginScreen(driver);
    }

    public returnScreen clickReturn() {

        skipToLogin.click();

        rtnlvbtn.click();

        return new returnScreen(driver);
    }

    public whistleScreen whistleblowing () {

        skipToLogin.click();

        Blow.click();

        return new whistleScreen(driver);
    }


}
