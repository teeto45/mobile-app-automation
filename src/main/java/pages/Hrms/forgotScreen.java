package pages.Hrms;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class forgotScreen  {


    public WebDriver driver;

    public forgotScreen(WebDriver driver) {

        this.driver = driver;

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @FindBy (xpath = "//*[@text='Enter email']") WebElement recoveryEmail;

    @FindBy (xpath = "//*[@text='Submit']") WebElement Submit;


    public void recoverPassword (String email){

        recoveryEmail.sendKeys(email);

        Submit.click();
    }
}
