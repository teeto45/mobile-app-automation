package pages.Hrms;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class returnScreen {

    public WebDriver driver;

    public returnScreen (WebDriver driver) {

        this.driver = driver;

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

//    @FindBy (xpath = "//*[@text='Return from Leave']']") WebElement rtnlvbtn;

    @FindBy( xpath = "//*[@text='Enter your Staff ID']") WebElement staffID;

    @FindBy (xpath = "(//*[@text='Password'])[2]") WebElement Pswrd;

    @FindBy (xpath = "//*[@text='Submit']") WebElement sbmt;


    public void details(String email, String password) {

        staffID.sendKeys(email);

        Pswrd.sendKeys(password);

        sbmt.click();

    };

}
