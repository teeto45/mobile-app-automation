package driverSetup;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.Hrms.homeScreen;

import java.net.URL;
import java.util.concurrent.TimeUnit;


public class baseClass {

    //Web driver is declared on a global scope to make it accessible to all functions/methods in the baseClass class

    WebDriver driver;


    @BeforeClass
    public homeScreen initializeDriver  () {

        try {

            //Major reasons for the try-catch is to stacktrace errors encountered when initializing driver in the console

            DesiredCapabilities capabilities = new DesiredCapabilities();

           //Created desiredcapabilities object as "capabilities" to handle all driver configurations

            capabilities.setCapability("BROWSER_NAME", "Android");

            //Set android deviceName desired capability

            capabilities.setCapability("deviceName", "emulator-5554");

            //Set android platformName desired capability to Android

            capabilities.setCapability("platformName", "Android");

            //Set android VERSION from device desired capability.

            capabilities.setCapability("platformVersion", "8.0.0");

            // Set android appPackage desired capability.

            capabilities.setCapability("appPackage", "com.hc_hub");

            // Set android appActivity desired capability.

            capabilities.setCapability("appActivity", ".MainActivity");

//            capabilities.setCapability("app", "src/main/resources/App/HRMS app-release 1.apk");


            capabilities.setCapability("unicodeKeyboard", true);


            capabilities.setCapability("resetKeyboard", true);

            // Set android autoAcceptAlerts to override all alerts

            capabilities.setCapability("autoAcceptAlerts", true);

            // Set android autoGrantPermissions to override all Permissions

            capabilities.setCapability("autoGrantPermissions", true);

            //This is to enable Appium API interact with connected device or emulator

            driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);



            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

            return new homeScreen(driver);

//            Lastly returned the homeScreen to enable initialization object in homeScreen page elements.

        } catch (Exception e) {
            System.out.println("Cause is : " + e.getCause());

            System.out.println("Cause is : " + e.getMessage());

            e.printStackTrace();
        }
        return null;
    }

    @AfterClass
    public void tearDown () {

        driver.quit();

    }
}

