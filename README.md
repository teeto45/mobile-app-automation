**HRMS-Mobile-Application Automation**
Fork and clone the repository to your local directory. git clone https://teeto45@bitbucket.org/teeto45/mobile-app
-automation.git
Ensure all Maven Dependencies are downloaded by running "mvn dependency:resolve"
Open src/main/java/driverSetup/baseClass.java to change desired capability's parameters such as device name and Apk
 root directory e.t.c
 
 **How to get device details**
 Plug in your device via any available usb port or download any desired AVD device emulator in Android Studio
 Run Terminal and type in "adb devices" to check list if online devices available
 Copy the device desired device info and replace in src/main/java/driverSetup/baseClass.java "deviceName"
 
 **How to get APK Info** 
 Launch APK on plugged in device
 Run "adb shell" in terminal
 Make sure the APK is running before running this command "dumpsys window windows | find "mCurrentFocus"". The above
 command focuses on your current screen to give APK info
 
 **Initializing Driver**
Web driver is declared on a global scope to make it accessible to all functions/methods in the baseClass class
Major reasons for the try-catch is to stacktrace errors encountered when initializing driver in the console 
Created desiredcapabilities object as "capabilities" to handle all driver configurations 
Lastly returned the homePage screen to enable link initialization with page elements. 
The linkage is made possible with the help of a constructor in th homePage screen. A "constructor" is a function that
 inherits the name of the class and is used to initialize objects. 
 
**Run test with TestNG **
Ensure Appium server is install and running. 
Select any test in src/test/java and run with testNG
To run all test in sequence, right-click on testng.xml and click run. 


